extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func allow_player_move():
	return true


var used = false
func _on_Area2D_area_entered(area):
	if not used:
		if area.get_parent().has_method("toggle_player_light"):
			used = true
			yield(area.get_parent().wait_to_finish_moving(), "completed")
			global.level_finished()
