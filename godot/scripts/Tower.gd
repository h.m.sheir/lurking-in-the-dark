extends Node2D


var levels = ["res://Rooms/Tutorial.tscn",
"res://Rooms/One.tscn",
"res://Rooms/Two.tscn",
"res://Rooms/Three.tscn",
"res://Rooms/Four.tscn",
"res://Rooms/Five.tscn"]


# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().paused = false
	get_node("CanvasLayer/Transition").visible = true
	
	for flame in get_node("flames").get_children():
		flame.visible = false
	var i = 1
	while i < global.setting_levels_finished:
		get_node("flames/"+str(i)).visible = true
		i += 1
	
	$tower/Clouds/AnimationPlayer.play("cloud_loop")
	
	var transitions = get_tree().get_nodes_in_group("transition_anim")
	var anim_node :AnimationPlayer = transitions[0]
	anim_node.play("go_out")
	yield(anim_node,"animation_finished")
	
	yield(get_tree().create_timer(1),"timeout")
	get_node("flames/"+str(global.setting_levels_finished)).visible = true
	$match_sfx.play()
	yield(get_tree().create_timer(1),"timeout")
	
	anim_node.play("go_in")
	yield(anim_node,"animation_finished")
	
	if(global.setting_levels_finished < levels.size()):
		get_tree().change_scene(levels[global.setting_levels_finished])
	else:
		# TODO: Show "Game Complete screen"
		var transitionsq = get_tree().get_nodes_in_group("gameover_anim")
		var anim_nodeq :AnimationPlayer = transitionsq[0]
		anim_nodeq.play("game_over")
		global.setting_levels_finished = 0
		Persist.save_game()
		yield(anim_nodeq,"animation_finished")
		get_tree().quit()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
