extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	visible = true
	modulate = Color.white
	$AnimationPlayer.play("Enter")
	get_tree().paused = true

var started = false
func _process(delta):
	if not started:
		if Input.is_action_just_pressed("toggle_candle"):
			get_tree().paused = false
			started = true
			$start_sfx.play()
			$AnimationPlayer.play("Fade")
