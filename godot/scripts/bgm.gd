extends Node


var fading_out = {}
# Called when the node enters the scene tree for the first time.
func _ready():
	for track in $tracks.get_children():
		fading_out[track.name] =false
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func fade_out(node_path, time = 2.5, reset = true):
	get_node("tracks/"+node_path+"/Tween").stop(get_node("tracks/"+node_path))
	get_node("tracks/"+node_path+"/Tween").interpolate_property(get_node("tracks/"+node_path), "volume_db", get_node(
	"tracks/"+node_path).volume_db, -60, time, Tween.TRANS_SINE,Tween.EASE_OUT)
	get_node("tracks/"+node_path+"/Tween").start()
	fading_out[node_path] = true
	yield(get_node("tracks/"+node_path+"/Tween"),"tween_completed")
	if fading_out[node_path]:
		if reset:
			get_node("tracks/"+node_path).stop()
	
func fade_in(node_path, time = 2.5, reset = true):
	fading_out[node_path] = false
	get_node("tracks/"+node_path+"/Tween").stop(get_node("tracks/"+node_path))
	if not get_node("tracks/"+node_path).is_playing() or reset:
		get_node("tracks/"+node_path).play()
	get_node("tracks/"+node_path+"/Tween").interpolate_property(get_node("tracks/"+node_path), "volume_db", get_node(
	"tracks/"+node_path).volume_db, 0, time, Tween.TRANS_SINE,Tween.EASE_IN)
	get_node("tracks/"+node_path+"/Tween").start()

func fade_chase_intensity(intensity = 0, time = 0.25):
	if intensity == 0:
		fade_out("chase_1", time, false)
		fade_out("chase_2", time, false)
		fade_out("chase_3", time, false)
	elif intensity == 1:
		fade_in("chase_1", time, false)
		fade_out("chase_2", time, false)
		fade_out("chase_3", time, false)
	elif intensity == 2:
		fade_in("chase_2", time, false)
		fade_out("chase_1", time, false)
		fade_out("chase_3", time, false)
	elif intensity == 3:
		fade_in("chase_3", time, false)
		fade_out("chase_2", time, false)
		fade_out("chase_1", time, false)
