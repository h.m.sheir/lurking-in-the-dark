extends Node

func _ready():
	load_game()
	
func save_game():
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	save_game.store_line(to_json(global.setting_levels_finished))
	save_game.close()
	
# Note: This can be called from anywhere inside the tree. This function
# is path independent.
func load_game():
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
		return # Error! We don't have a save to load.
	
	# We need to revert the game state so we're not cloning objects
	# during loading. This will vary wildly depending on the needs of a
	# project, so take care with this step.
	# For our example, we will accomplish this by deleting savable objects.
#	var save_nodes = get_tree().get_nodes_in_group("Persist")
#	for i in save_nodes:
#		i.queue_free()

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open("user://savegame.save", File.READ)
	while not save_game.eof_reached():
		var current_line = parse_json(save_game.get_line())
		# Firstly, we need to create the object and add it to the tree and set its position.
		if current_line:
			global.setting_levels_finished = int(current_line)
#			var object_pack = load(current_line["filename"])
#			if load(current_line["filename"]):
#				var new_object = object_pack.instance()
#				get_node(current_line["parent"]).add_child(new_object)
#				if "position" in new_object:
#					new_object.position = Vector2(current_line["pos_x"], current_line["pos_y"])
#				# Now we set the remaining variables.
#				for i in current_line.keys():
#					if i == "filename" or i == "parent" or i == "pos_x" or i == "pos_y":
#						continue
#					new_object.set(i, current_line[i])
#				if new_object.has_method("load_globals"):
#					new_object.load_globals()
	save_game.close()
	if global.setting_levels_finished > 0:
		global.force_change_scene()
